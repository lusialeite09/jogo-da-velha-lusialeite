
var jogador = 0;
var jogadas = 0;

function clique(id) {
	
	var tag = null;
	
	if(id.currentTarget)
		tag = id.currentTarget;
	else
		tag = document.getElementById('casa'+id);

	if(tag.style.backgroundImage == '' || tag.style.backgroundImage == null)
	{
		
		var endereco = 'img/'+jogador+'.jpg';
		tag.style.backgroundImage = 'url('+endereco+')';
		jogadas += 1;

		if (jogadas >=5 ){
			var ganhou = verificarGanhador();
		}
		
		switch(ganhou)
		{
			case 1:
				if(jogador == 0)
				{
					finalizar(jogador);
				}
				else
				{
					finalizar(jogador);
				}
				break;

			case -1:
				{
					finalizar(2);
				}
				break;
			case 0:

				break;
		}

		vez = document.getElementById('vez');
		if(jogador == 0)
		{
			jogador = 1;
			vez.innerHTML = 'Zeca Urubu';
		}else
		{
			jogador = 0;
			vez.innerHTML = 'Pica-Pau';
		}
	}
}

function verificarGanhador(){
	c1 = document.getElementById('casa1');
	c1 = c1.style.backgroundImage;
	console.log(c1);
	c2 = document.getElementById('casa2');
	c2 = c2.style.backgroundImage;
	c3 = document.getElementById('casa3');
	c3 = c3.style.backgroundImage;
	c4 = document.getElementById('casa4');
	c4 = c4.style.backgroundImage;
	c5 = document.getElementById('casa5');
	c5 = c5.style.backgroundImage;
	c6 = document.getElementById('casa6');
	c6 = c6.style.backgroundImage;
	c7 = document.getElementById('casa7');
	c7 = c7.style.backgroundImage;
	c8 = document.getElementById('casa8');
	c8 = c8.style.backgroundImage;
	c9 = document.getElementById('casa9');
	c9 = c9.style.backgroundImage;
	
	if( (c1==c2 && c2==c3 && c1!='') ||
		(c4==c5 && c5==c6 && c4!='') ||
		(c7==c8 && c8==c9 && c7!='') ||
		(c1==c4 && c4==c7 && c1!='') ||
		(c2==c5 && c5==c8 && c2!='') ||
		(c3==c6 && c6==c9 && c3!='') ||
		(c1==c5 && c5==c9 && c1!='') ||
		(c3==c5 && c5==c7 && c3!=''))
	{
		return 1;

	}else if(jogadas == 9){
		return -1;
	}
	return 0;
}

function finalizar(player){
	if (player == 0)
	{
		var vencedor = document.getElementById('vencedor');
		vencedor.innerHTML = '<h2 class="text-white">Pica-pau ganhou!</h2><button onclick = "iniciar(0)" type="button" class="btn btn-success m-3">Jogar de Novo!</button>';
		var efeito = document.getElementById('efeito');
		efeito.play();
	}
	if (player == 1)
	{
		var vencedor = document.getElementById('vencedor');
		vencedor.innerHTML = '<h2 class="text-white">Zeca urubu ganhou!</h2><button onclick = "iniciar(1)" type="button" class="btn btn-success m-3">Jogar de Novo!</button>';
		var efeito = document.getElementById('efeito');
		efeito.play();
	}
	if (player == 2)
	{
		var vencedor = document.getElementById('vencedor');
		vencedor.innerHTML = '<h2 class="text-white">Deu empate!</h2 class="mt-4"><button onclick = "iniciar(2)" type="button" class="btn btn-success m-3">Jogar de Novo!</button';
		var efeito = document.getElementById('efeito');
		efeito.play();
	}

	tags = document.getElementsByClassName('casa');

	for(i=0; i<9; i++){
		tags[i].onclick = null;
	}
}

function iniciar(jogador) {
	var vencedor = document.getElementById('vencedor');
	vencedor.style.height="0%";
	vencedor.innerHTML = '';
	if (jogador == 0){
		vez = document.getElementById('vez');
		vez.innerHTML = 'Mihawk';
		jogador = 1;
	}else if(jogador == 1) {
		vez = document.getElementById('vez');
		vez.innerHTML = 'Shanks';
		jogador = 0;
	} else {
		jogador = 0;
	}
	jogadas = 0;

	casas = document.getElementsByClassName('casa');

	for(i=0; i<9; i++){
		casas[i].onclick=clique;
		casas[i].style.backgroundImage='';
	}

}

function musica(){
	var audio = document.getElementById('musicafundo');
	if (audio.className == "desactive"){
		audio.play();
		audio.className = "active";
	} else {
		audio.pause();
		audio.currentTime = 0;
		audio.className = "desactive";
	}
}
